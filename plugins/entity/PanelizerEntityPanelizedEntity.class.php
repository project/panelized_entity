<?php

/**
 * @file
 * Class for the Panelized entity plugin.
 */

/**
 * Panelizer Entity plugin class.
 */
class PanelizerEntityPanelizedEntity extends PanelizerEntityDefault {
  public $entity_admin_root = 'panelized-entity/%panelized_entity_uuid';
  public $entity_admin_bundle = 'panelized_entity';
  public $views_table = 'panelized_entity';
  public $uses_page_manager = TRUE;
  public $supports_revisions = TRUE;

  /**
   * {@inheritdoc}
   */
  public function entity_access($op, $entity) {
    // This must be implemented by the extending class.
    if ($op == 'view') {
      return TRUE;
    }

    return user_access('administer panelized_entity entities');
  }

  /**
   * Implement the save function for the panelized_entity entity.
   *
   * @param object $entity
   *   The panelized_entity entity to be saved.
   */
  public function entity_save($entity) {
    entity_save('panelized_entity', $entity);
  }

  /**
   * Returns the bundle label.
   *
   * @return null|string
   *   Bundle label.
   */
  public function entity_bundle_label() {
    return t('Panelized Entity');
  }

  /**
   * Determine if the entity allows revisions.
   */
  public function entity_allows_revisions($entity) {
    $access = user_access('administer panelized_entity entities');
    return array(TRUE, $access, TRUE);
  }

  /**
   * Implements a delegated hook_page_manager_handlers().
   *
   * This makes sure that all panelized entities have the proper entry
   * in page manager for rendering.
   */
  public function hook_default_page_manager_handlers(&$handlers) {
    $handler = new stdClass();
    $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
    $handler->api_version = 1;
    $handler->name = 'panelized_entity_view_panelizer';
    $handler->task = 'panelized_entity_page_view';
    $handler->subtask = '';
    $handler->handler = 'panelizer_node';
    $handler->weight = -100;
    $handler->conf = array(
      'title' => t('Panelized Entity panelizer'),
      'context' => 'argument_entity_id:panelized_entity_1',
      'access' => array(),
    );
    $handlers['panelized_entity_view_panelizer'] = $handler;

    return $handlers;
  }

  /**
   * {@inheritdoc}
   */
  public function preprocess_panelizer_view_mode(&$vars, $entity, $element, $panelizer, $info) {
    $vars['element']['#panelized_entity'] = $vars['element']['#entity'];
    parent::preprocess_panelizer_view_mode($vars, $entity, $element, $panelizer, $info);
  }

}
