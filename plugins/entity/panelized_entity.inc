<?php

/**
 * @file
 * Definition of the Panelized Entity plugin.
 */

$plugin = array(
  'handler' => 'PanelizerEntityPanelizedEntity',
  'entity path' => 'panelized-entity/%panelized_entity_uuid',
  'uses page manager' => TRUE,
  'hooks' => array(
    'permission' => TRUE,
    'default_page_manager_handlers' => TRUE,
    'panelizer_defaults' => TRUE,
  ),
);
