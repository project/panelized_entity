<?php

/**
 * @file
 * Panelizer default configuration for panelized_entity_block_test.
 */

/**
 * Implements hook_panelizer_defaults().
 */
function panelized_entity_block_test_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'panelized_entity';
  $panelizer->panelizer_key = 'test_with_block';
  $panelizer->access = array();
  $panelizer->view_mode = 'default';
  $panelizer->name = 'panelized_entity:test_with_block:default:default';
  $panelizer->css_id = '';
  $panelizer->css_class = '';
  $panelizer->css = '';
  $panelizer->no_blocks = FALSE;
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $panelizer->pipeline = 'ipe';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'DEFAUL BLOCK TITLE';
  $display->uuid = '32c85f63-3e95-45b8-827b-6a20c0e5d2cc';
  $display->storage_type = 'panelizer_default';
  $display->storage_id = 'panelized_entity:test_with_block:default:default';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-4af8d0f3-a43b-404b-9287-0c1d69d1fe35';
  $pane->panel = 'middle';
  $pane->type = 'block';
  $pane->subtype = 'system-powered-by';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '4af8d0f3-a43b-404b-9287-0c1d69d1fe35';
  $display->content['new-4af8d0f3-a43b-404b-9287-0c1d69d1fe35'] = $pane;
  $display->panels['middle'][0] = 'new-4af8d0f3-a43b-404b-9287-0c1d69d1fe35';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['panelized_entity:test_with_block:default:default'] = $panelizer;

  return $export;
}
