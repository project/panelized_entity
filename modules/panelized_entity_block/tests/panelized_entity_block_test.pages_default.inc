<?php

/**
 * @file
 * Packaged page manager instance for testing of panelized_entity_block.
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function panelized_entity_block_test_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'panelized_entity_block';
  $page->task = 'page';
  $page->admin_title = 'Panelized Entity Block';
  $page->admin_description = '';
  $page->path = 'panelized-entity-block';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_panelized_entity_block__panel';
  $handler->task = 'page';
  $handler->subtask = 'panelized_entity_block';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '70e2c801-360e-4d8d-85ec-1e335607a65f';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_panelized_entity_block__panel';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-57003f28-a2d3-4cbb-9810-af48fece8674';
  $pane->panel = 'middle';
  $pane->type = 'block';
  $pane->subtype = 'panelized_entity_block-pe_test_with_block';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '57003f28-a2d3-4cbb-9810-af48fece8674';
  $display->content['new-57003f28-a2d3-4cbb-9810-af48fece8674'] = $pane;
  $display->panels['middle'][0] = 'new-57003f28-a2d3-4cbb-9810-af48fece8674';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-57003f28-a2d3-4cbb-9810-af48fece8674';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['panelized_entity_block'] = $page;

  return $pages;
}
