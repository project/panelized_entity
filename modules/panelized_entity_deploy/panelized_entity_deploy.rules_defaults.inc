<?php

/**
 * @file
 * Deploy_addon_content.rules_defaults.inc.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function panelized_entity_deploy_default_rules_configuration() {
  $items = array();
  if (module_exists('deploy_adhoc_plan')) {
    $items['rules_panelized_entity_deploy_add_to_ad_hoc_deploy_plan'] = entity_import('rules_config', '{ "rules_panelized_entity_deploy_add_to_ad_hoc_deploy_plan" : {
        "LABEL" : "Add Panelized entity to User Deploy Plan",
        "PLUGIN" : "action set",
        "OWNER" : "rules",
        "REQUIRES" : [ "deploy_adhoc_plan" ],
        "USES VARIABLES" : { "panelized_entity" : { "label" : "Panelized Entity", "type" : "panelized_entity" } },
        "ACTION SET" : [
          { "deploy_adhoc_plan_rules_action_add_to_managed_plan" : { "entity" : [ "panelized_entity" ] } }
        ]
      }
    }');

    $items['rules_panelized_entity_remove_ad_hoc_deploy_plan'] = entity_import('rules_config', '{ "rules_panelized_entity_remove_ad_hoc_deploy_plan" : {
        "LABEL" : "Remove Panelized Entity from User Deploy Plan",
        "PLUGIN" : "action set",
        "OWNER" : "rules",
        "REQUIRES" : [ "deploy_adhoc_plan" ],
        "USES VARIABLES" : { "panelized_entity" : { "label" : "Panelized Entity", "type" : "panelized_entity" } },
        "ACTION SET" : [
          { "deploy_adhoc_plan_manager_action_delete_from_plan" : { "entity" : [ "panelized_entity" ] } }
        ]
      }
    }');
  }

  $items['rules_panelized_entity_deploy_add_to_push_to_next_deploy_plan'] = entity_import('rules_config', '{ "rules_panelized_entity_deploy_add_to_push_to_next_deploy_plan" : {
      "LABEL" : "Add Panelized Entity to Push to Next Deploy Plan",
      "PLUGIN" : "action set",
      "OWNER" : "rules",
      "REQUIRES" : [ "deploy" ],
      "USES VARIABLES" : { "panelized_entity" : { "label" : "Panelized Entity", "type" : "panelized_entity" } },
      "ACTION SET" : [
        { "deploy_rules_action_add_to_managed_plan" : { "plan_name" : "push_to_next", "entity" : [ "panelized_entity" ] } }
      ]
    }
  }');

  $items['rules_panelized_entity_deploy_remove_push_to_next_deploy_plan'] = entity_import('rules_config', '{ "rules_panelized_entity_deploy_remove_push_to_next_deploy_plan" : {
      "LABEL" : "Remove Panelized Entity from Push to Next Deploy Plan",
      "PLUGIN" : "action set",
      "OWNER" : "rules",
      "REQUIRES" : [ "deploy" ],
      "USES VARIABLES" : { "panelized_entity" : { "label" : "Panelized Entity", "type" : "panelized_entity" } },
      "ACTION SET" : [
        { "deploy_manager_action_delete_from_plan" : { "plan_name" : "push_to_next", "entity" : [ "panelized_entity" ] } }
      ]
    }
  }');

  return $items;
}
