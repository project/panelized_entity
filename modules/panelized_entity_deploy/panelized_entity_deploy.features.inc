<?php

/**
 * @file
 * panelized_entity_deploy.features.inc
 */

/**
 * Implements hook_views_api().
 */
function panelized_entity_deploy_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
